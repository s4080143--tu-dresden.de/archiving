#!/bin/bash

#SBATCH --job-name=pack
#SBATCH --time=168:00:00
#SBATCH --mail-type=all
#SBATCH --cpus-per-task=12
#SBATCH --ntasks=1

# Author: Bastian Loehrer, PSM, TUD. bastian.loehrer@tu-dresden.de
# 18 January 2021
#
# Pack directory ${source_folder} into compressed tarballs
# - ${output_name}.tar.gz_part00
# - ${output_name}.tar.gz_part01
# - ${output_name}.tar.gz_part02
# - ...
# and write a file list to ${output_name}.txt
#
# To unpack from these files do the following from within an empty directory:
# cat ../${output_name}.tar.gz_part* | tar -xvz

usage="USAGE: bash pack_for_archive.sh <path/to/source> </path/to/desired/output/file> <optional tar options>"

if [ -z $1 ] ; then
  echo "${usage}" && exit 1;
fi

if [ -z $2 ] ; then
  echo "${usage}" && exit 2;
fi

fsize="500G"        # target size of split files
source_folder=$1    # source folder
output_name=$2      # prefix of the target file (i.e. file path without suffix)
taroptions=${*:3 }  # Optional additional arguments are passed on to the tar command

echo "Script started at $(date)."

module load pigz 2>&1

# exit when any command fails (usually due to incorrect paths)
set -e
# NB: This setting has the script stop also when $output_name points to a file
#     inside a non-existing directory. In this case realpath($output_name) below
#     will throw an error and $output_name would be unset.

# Set the dotglob option, such that * matches hidden files as well (but not .., .)
shopt -s dotglob

# get the resolved absolute paths
source_folder=$(realpath $source_folder)
output_name=$(realpath $output_name)

# Check if source path exists and is directory
if [ ! -e $source_folder ]; then
 echo "source_folder does not exist: ${source_folder}"
 echo "Exiting."
 exit
fi 
if [ ! -d $1 ]; then
 echo "source_folder is not a directory: ${source_folder}"
 echo "Exiting."
 exit
fi 

echo "Current directory   = ${source_folder}"
echo "taroptions          = ${taroptions}"
echo "output_name         = ${output_name}"
echo "SLURM_CPUS_PER_TASK = ${SLURM_CPUS_PER_TASK}"
echo "fsize               = ${fsize}"

# Pack from within the source directory to omit the relative path to the directory
# Instead, one could use `tar cv -C ${source_folder} .` below, but this results in a 
# leading directory `.` which is ugly
wdir=$(pwd)
cd $source_folder

# Write header to file list
echo "# This archive contains files originally stored at:" >> ${output_name}.txt
echo "# ${source_folder}"                                  >> ${output_name}.txt
echo "# Compression started:"                              >> ${output_name}.txt
echo "# $(date)"                                           >> ${output_name}.txt
echo ""                                                    >> ${output_name}.txt

# Actually pack the files, also writing a file list
cmd='srun tar ${taroptions} -cv * 2>>${output_name}.txt | pigz -9 -p ${SLURM_CPUS_PER_TASK} - | split -d -b $fsize - ${output_name}.tar.gz_part'
echo "${cmd}"
eval $cmd

# Write footer to the file list
echo ""                                                    >> ${output_name}.txt
echo "# Compression ended:"                                >> ${output_name}.txt
echo "# $(date)"                                           >> ${output_name}.txt

cd $wdir

echo "Script finished at $(date)."
