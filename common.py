#!/usr/bin/env python3

""" Common functions shared among different programs
"""

import os
import re
import glob
from file_read_backwards import FileReadBackwards
import warnings


def argsort(seq):
    # https://stackoverflow.com/a/3382369/7042795
    return sorted(range(len(seq)), key=lambda i: seq[i])


def get_basefilepaths(src, include_unverified=False):
    """ Get qualified file base paths 
    - For each filename
        1) a `{basefilename}.tar.gz` or `{basefilename}.tar.gz_part00` must exist.
        2) the `{basefile}.sha256` or `{basefile}.md5` must end with `# Done.`.
      This ensures that only validated archives are synced.
    """
    # Collect basefilepaths
    basefilepaths = []
    for path, subdirs, files in os.walk(src):
        for name in files:
            match = re.match(r"^(.*)\.tar\.gz(?:_part00)?$", name)  # https://regex101.com/r/L6XFee/1
            if match:
                basefilename, = match.groups()
                basefilepaths.append(os.path.join(path, basefilename))
    
    assert len(basefilepaths) == len(set(basefilepaths))
    
    # Check if files are completely packed
    if include_unverified:
        basefilepaths_completed = []
        for basefilepath in basefilepaths:
            path_txt = f"{basefilepath}.txt"
            if not os.path.isfile(path_txt):
                warnings.warn(f"{path_txt} missing. Assuming the archive file is complete.")
                basefilepaths_completed.append(basefilepath)
            else:
                with FileReadBackwards(path_txt, encoding="utf-8") as frb:
                    l = frb.readline().strip()
                    l = frb.readline().strip()
                if l == "# Compression ended:":
                    basefilepaths_completed.append(basefilepath)
                else:
                    print(f"Skipping (not completed):            {basefilepath}")
        basefilepaths = basefilepaths_completed
    else:
        # Not needed, because will be checking if fully verified
        pass
    
    # Check if files are fully verified
    if not include_unverified:
        basefilepaths_verified = []
        for basefilepath in basefilepaths:
            path_sums = [f"{basefilepath}.{hashtype}" for hashtype in ['sha256', 'md5']]
            path_sums = [f for f in path_sums if os.path.isfile(f)]
            if len(path_sums) == 0:
                print(f"Skipping (not verified):            {basefilepath}")
            elif len(path_sums) > 1:
                print(f"Skipping (multiple checksum files): {basefilepath}")
            else:
                path_sums = path_sums[0]
                with FileReadBackwards(path_sums, encoding="utf-8") as frb:
                    l = frb.readline().strip()
                if l == "# Done.":
                    basefilepaths_verified.append(basefilepath)
                else:
                    print(f"Skipping (not fully verified):      {basefilepath}")
        basefilepaths = basefilepaths_verified
    
    return basefilepaths


def get_files(basefilepath, ext_ignore=['txt']):
    """ Get archive files for a basefilepath
    """
        
    # Get list of qualifying files for the basefilepath
    files = []
    for path in glob.glob(f"{basefilepath}*"):
        suffix = path[len(basefilepath):]
        if suffix.startswith('.'):
            files.append(path)
        else:
            match = re.match(r"^_[0-9]+\..*$", suffix)  # https://regex101.com/r/GZyKzg/1
            if match:
                files.append(path)
            else:
                #print(f"{basefilepath} excl. {path}")
                pass

    # Collect files
    
    files_tar_part = [];  files_tar_part_numbers = []
    files_tar = []
    files_aux = []

    for path in files:
        
        # Identify tar..._part... files
        match = re.match(r"^(?:.*)\.(?:tar(?:\.gz)?)_part(\d+)$", path)  # https://regex101.com/r/dVprg3/3
        if match:
            files_tar_part.append(path)
            files_tar_part_numbers.append(int(match.group(1)))
            continue
        
        # Identify tar... files (single, without part suffix)
        match = re.match(r"^(?:.*)\.(?:tar(?:\.gz)?)$", path)  # https://regex101.com/r/uw7arg/1
        if match:
            files_tar.append(path)
            continue
        
        # Identify auxiliary files
        match = re.match(r"^(.*)(?<!\.tar)\.((?:tar\.)?[^.]+)$", path)  # https://regex101.com/r/24BtZC/1
        if match:
            extension = match.group(2)
            if extension in ext_ignore:
                continue
            else:
                files_aux.append(path)
    
    files = []
    if len(files_tar) > 1:
        print(f"Skipping! Multiple tar files: {files_tar!s}")
        return None
    elif len(files_tar_part) > 0 and len(files_tar) > 0:
        print(f"Skipping! Multiple tar files: {files_tar!s}, {files_tar_part!s}")
        return None
    elif len(files_tar_part) == 0 and len(files_tar) == 1:
        files.extend(files_tar)
    elif len(files_tar_part) > 0 and len(files_tar) == 0:
        if len(files_tar_part) != max(files_tar_part_numbers) + 1:
            print(f"Skipping! Part tar files missing: {files_tar_part!s}")
            return None
        else:
            files.extend(files_tar_part)
    
    files.extend(files_aux)

    return files
