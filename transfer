#!/usr/bin/env python3

""" This program transfers archive files into the archive, via dtrsync.
"""
__author__ = "Bastian Loehrer"
__copyright__ = "Bastian Loehrer"
__maintainer__ = "Bastian Loehrer"
__email__ = "bastian.loehrer@tu-dresden.de"
__status__ = "Experimental"

import os
import re
import subprocess
import common

def main():
    
    import argparse

    parser = argparse.ArgumentParser( description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter )
    parser.add_argument('src', type=str, help='Source directory.')
    parser.add_argument('dst', type=str, help='Destination (in archive).')
    parser.add_argument('--ext-ignore', type=str, nargs='+', help='Files with these extensions are ignored (e.g. txt for *.txt files).')
    parser.add_argument('--dry', action='store_true', help='Dry run.')
    parser.add_argument('--include-unverified', action='store_true', help='Also transfer non-verified data.')
    args = parser.parse_args()

    return transfer(**vars(args))


def transfer(src, dst, ext_ignore=None, dry=False, include_unverified=False):
    """ Transfer files via `dtrsync -avvh --relative src/{filename} dst`, 
    with `filename = {basefilename}.{ext}`.
    - For each filename
        1) a `{basefilename}.tar.gz` or `{basefilename}.tar.gz_part00` must exist.
        2) the `{basefile}.sha256` or `{basefile}.md5` must end with `# Done.`.
      This ensures that only validated archives are synced.
    - A single job is launched for each file, with the `--dependency` flag to 
      ensure sequential processing.
    - Files with any extension listed in `ext_ignore` are not synced.
    - ext_ignore -- Files with these extensions are ignored.
    """
    
    if ext_ignore is None:
        ext_ignore = []
    
    basefilepaths_verified = common.get_basefilepaths(src, include_unverified=include_unverified)
    
    # Transfer files
    jobid = None
    for path, subdirs, files in os.walk(src):
        # identify subdir inside src
        subpath = os.path.relpath(path, src)
        for name in files:
            match = re.match(r"^(.*)(?<!\.tar)\.((?:tar\.)?[^.]+)$", name)  # https://regex101.com/r/24BtZC/1
            if match:
                basename, extension = match.groups()
                basefilepath = os.path.join(path, basename)
                if basefilepath not in basefilepaths_verified:
                    # this also skips those files with _12345689.log appended, etc.
                    continue
                if extension in ext_ignore:
                    continue
                assert ' ' not in path
                assert ' ' not in name
                dependency_str = f"--dependency=afterany:{jobid}" if jobid else ""
                
                # src_ is source path to file with dot inserted from where the relative path in dst is taken
                # See https://askubuntu.com/a/552122
                src_ = os.path.join(src, '.' if subpath != '.' else '', subpath, name)
                cmd = ["dtrsync", dependency_str, "-avvh", "--relative", src_, dst]
                
                cmd_str = ' '.join(cmd)
                
                if dry:
                    print(cmd_str)
                else:
                    if True:
                        result = os.popen(cmd_str)
                        output = result.read()
                    else:
                        # this should work as well but has shown weird behavior
                        result = subprocess.run(cmd, stdout=subprocess.PIPE)
                        output = result.stdout.decode('utf-8')
                    jobid, = re.match(r"^Submitted batch job (\d+)$", output.strip()).groups()
                    print(jobid, cmd_str)
                
            else:
                raise NotImplementedError
    
    return


if __name__ == "__main__":
    import sys
    sys.exit(main())
