# Archiving Data


## 0 Data structure

Conceptualize a data structure that makes sense.


## 1 Pack data

Archived files should not exceed 500GB. Therefore, simulation data normally needs to be packed in chunks. 
The SLURM bash job [pack_for_archive.sh](./pack_for_archive.sh) will do that for you.

Usage:
```bash
sbatch pack_for_archive.sh /path/to/source/folder ./data/meaningfulname
```
will create 
- `data/meaningfulname.tar.gz_part00` → archive parts with max. 500GB
- `data/meaningfulname.tar.gz_part01` → archive parts with max. 500GB
- ...
- `data/meaningfulname.tar.txt` → Information, List of files, Success message at the end.

If your packed data does not exceed the 500GB and you end up with a single file `data/meaningfulname.tar.gz_part00` you can safely rename it to `data/meaningfulname.tar.gz`.

Keep the scripts and store them with the archived data. This way it will always be clear how archived data was produced.

Additional arguments are passed on to the tar command.
This can be useful, to exclude some data that should not go into the archive or when intending to pack these data separately.
For example,
```bash
sbatch pack_for_archive.sh /path/to/source/folder ./data/meaningfulname --exclude='**/*.bin' --exclude='**/*.bin.info' --exclude='**/*.res' --exclude='**/*.res.info' --exclude='results*/**/*.dat' --exclude='**/*.h5'
```
The opposite behaviour is unfortunately not available, i.e. there is no `--include` option in GNU tar.
You would have to do `find` and pipe to `tar`, but that's probably not worth it.
https://stackoverflow.com/a/18731818/7042795

So when packing simulation directories, I would just pack everything into one archive, and then also pack a really small version without any simulation data.
The small amount of redundancy is negligible, but it will allow one to quickly check out a simulation setup without having to download all the simulation data.

## 2 Check data

→ [tarsum](./tarsum)

Launch the following batch job to have the files in the (split) archives checked
against the original files.
```bash
sbatch --output=meinarchiv_%j.out --error=meinarchiv_%j.err tarsum \
  --checksum sha256 --output meinarchiv.sha256 \
  --validate path/to/folder/with/original/files \
  meaningfulname.tar.gz
```

- The program tarsum will look for `meaningfulname.tar.gz` and if it does not 
find such a file it will search for files named `meaningfulname.tar.gz_part*`
- Instead of `sha256` you can use any other hash sum supported by python's hashlib (e.g. `md5`)
- `tarsum` needs the [split_file_reader](https://pypi.org/project/split-file-reader/) module to process split archives.

`tarsum` will go through the files in `meinarchiv.tar.gz` (without actually unpacking the archive) and do the following:

1. Compute check sum for the file in the archive.
2. Compute check sum for the original file.  
   If none can be found, an error message is issued
3. Compare both sums.  
   If they are different, an error message is issued.
4. Write the hash (of the file in the archive) to the output file.

If the program is canceled before being done with all the files you can simply 
relaunch it and reuse the previous output file.  
Files already checked 
— i.e. for which hash sums are already present in the output file —
are skipped and the additional hashes are appended to the output file.


## 3 Verzeichnis für Archivierung vorbereiten

1. Schritt 1 und 2 für alle zu archivierende Verzeichnisse wiederholen.
2. Eine erklärende Klartextdatei ablegen, die die Datenstruktur erläutert.
3. Zuletzt Prüfsummen für alle zu archivierenden Daten berechnen und abspeichern:
  ```bash
  sha256sum * > checksums_sha256.txt
  ```

Es liegt damit folgende Ordnerstruktur vor:

| Datei                       | Grund, Nutzen    |
|-----------------------------|------------------|
| `meinarchiv1.tar.gz`        | Gepackte Daten 1 |
| `meinarchiv1.sha256`        | Prüfsummen für einzelne Dateien in gepackte Daten 1 → ermöglicht es, später zu prüfen, ob eine Datei fehlerfrei entpackt wurde |
| `meinarchiv2.tar.gz`        | Gepackte Daten 2 |
| `meinarchiv2.sha256`        | Prüfsummen für einzelne Dateien in gepackte Daten 2 |
| ...                         | ...              |
| `Readme.txt` o.ä.           | Erklärung zu den Daten |
| `checksums_sha256.txt`      | Prüfsummen der obigen Dateien |


## 4 Transfer the packed data

Using the `tranfer` program...
Needs pip module [`file_read_backwargs`](https://file-read-backwards.readthedocs.io/en/latest/installation.html)


# Retrieving data from the archive

The following script retrieves archived data and unpacks it in step two.
Getting individual files, instead of unpacking the whole, is also possible.


```bash
#!/bin/bash

# This script downloads archived data 
#   from ${archive_path}/${basename}.* (in the archive file system)
#   to   ./${basename}.*
# and then extracts the downloaded tar files to
#   extracted_path

basename="f_bulk_beta_dx_36_cfl_08_ds_1h"
archive_path="/archiv/p_lza_psm/99_xyz/data/case_xyz/simulation_results"
extracted_path="./${basename}"

# ------------------------------------------------------------------------------
# Assemble a list of files to get from the archive
# ------------------------------------------------------------------------------
# Files must be provided explicitely, because the loginservers do not have access to the archive file system.
files=""
files="${files} ${archive_path}/${basename}.sha256"
files="${files} ${archive_path}/${basename}.tar.gz_part00"
files="${files} ${archive_path}/${basename}.tar.gz_part01"

DEPENDENCY=""

# ------------------------------------------------------------------------------
# Step 1 download data from the archive
# ------------------------------------------------------------------------------
# Issuing separate transfers for each file to allow the
# controller of the tape-based archive file system to hold back
# file transfers when it would otherwise be overwhelmed.
for file_in_archive in $files; do
    JOB_CMD="dtrsync"
    if [ -n "$DEPENDENCY" ] ; then
        JOB_CMD="$JOB_CMD --dependency=afterok:$DEPENDENCY"
    fi
    JOB_CMD="$JOB_CMD -avh ${file_in_archive} ."
    echo -n "Running command: $JOB_CMD  "
    OUT=`$JOB_CMD`
    echo "Result: $OUT"
    DEPENDENCY=`echo $OUT | awk '{print $4}'`
done

# ------------------------------------------------------------------------------
# Step 2 extract the tar files
# ------------------------------------------------------------------------------
mkdir -p $extracted_path
sbatch --dependency=afterok:$DEPENDENCY archiving/unpack.sh basename.tar.gz $extracted_path
```
