#!/bin/bash

#SBATCH --job-name=unpack
#SBATCH --time=168:00:00
#SBATCH --cpus-per-task=12
#SBATCH --ntasks=1

usage="USAGE: bash unpack.sh <path/to/source.tar.gz> <path/to/output/directory>"

usage="Extract a gzipped split tar archive.
Author: Bastian Loehrer, PSM, TUD. bastian.loehrer@tu-dresden.de, August 2023

Usage: bash extract_split.sh FILE DIRECTORY

    FILE is a path path/to/source.tar.gz even when there are multiple
         path/to/source.tar.gz_part00, path/to/source.tar.gz_part01, ...
    DIRECTORY is the path to the output directory to where the archived
         files are extracted.

All files path/to/source.tar.gz* will be concatenated, then unzipped
with multiple processes.
"

if [ -z $1 ] ; then
  echo "${usage}" && exit 1;
fi

if [ -z $2 ] ; then
  echo "${usage}" && exit 2;
fi

if [ $3 ] ; then
  echo "${usage}" && exit 2;
fi

src=$1       # source folder
dst=$2       # prefix of the target file (i.e. file path without suffix)

echo "Script started at $(date)."

module load pigz 2>&1

# exit when any command fails (usually due to incorrect paths)
set -e

# get the resolved absolute paths
src=$(realpath $src)
dst=$(realpath $dst)

echo "src         = ${src}*"
echo "dst         = ${dst}"
echo "# processes = ${SLURM_CPUS_PER_TASK}"

wdir=$(pwd)
cd $dst

# Actually pack the files, also writing a file list
# cmd='srun cat ${src}* | tar xvzf -'
cmd='srun cat ${src}* | pigz -dc -p ${SLURM_CPUS_PER_TASK} | tar --skip-old-files -xvf -'
echo "${cmd}"
eval $cmd

cd $wdir

echo "Script finished at $(date)."
